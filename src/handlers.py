import re
import requests
from bs4 import BeautifulSoup
import json
import sys
import json
import yaml


class Followers:
    """
    """

    _query_hash_pattern = r'const t\=\"\w+\"'

    _list_url = '{}/graphql/query/?query_hash={}&variables={}'

    _targets = dict()

    _items = dict()

    _variables = dict()

    def __fill_query_hash_for(self, targets: list):
        """ """
        for target in targets:
            url = f'{self._host}/{target}/'
            response = self._session.get(url)
            if not url == response.url:
                raise Exception('Application can`t login, check `sessionid` parameter.')
            soup = BeautifulSoup(response.text, 'html.parser')
            for link in soup.head.find_all('link'):
                href = link.get('href')
                if 'es6/Consumer.js' in href:
                    response = self._session.get(self._host + href)
                    matches = re.findall(self._query_hash_pattern, response.text)
                    if len(matches) > 1:
                        self._targets[target] = matches[1][9:-1]
                    break

    def __fill_variables_for(self, targets: list):
        """ """
        for target in targets:
            url = f'{self._host}/{target}/?__a=1'
            response = self._session.get(url).json()
            self._variables[target] = dict(id=response.get('graphql', {}).get('user', {}).get('id'),
                                           include_reel=False, fetch_mutual=False, first=50)
        try:
            for target, last_after in yaml.load(open(self._config_afters_cache), Loader=yaml.FullLoader).items():
                self._variables[target]['after'] = last_after
        except AttributeError as e:
            pass

    def __fill_items_from_storage(self):
        """ """
        with open(self._followers_output) as f:
            try: self._items = json.load(f)
            except: self._items = dict()

    def __init__(self, config):
        """ """
        self._host = config.host
        self._config_afters_cache = config.followers_afters_cache
        self._followers_output = config.followers_output
        self._session = requests.Session()
        self._session.headers.update(cookie=f'sessionid={config.sessionid}',
                                     host=self._host.split('/')[-1],
                                     user_agent=config.user_agent)
        try:
            self.__fill_query_hash_for(config.targets)
            self.__fill_variables_for(config.targets)
            self.__fill_items_from_storage()
        except Exception as e:
            print(e)

    def __iter__(self):
        """ """
        for target, query_hash in self._targets.items():
            repeat = True
            while repeat:
                url = self._list_url.format(self._host, query_hash,
                                            json.dumps(self._variables[target]))
                try:
                    response = self._session.get(url).json()
                    sys.stdout.flush()
                    data = response.get('data', {})\
                                   .get('user', {})\
                                   .get('edge_followed_by', {})
                    profiles = data.get('edges', [])
                    repeat = data.get('page_info', {})\
                                 .get('has_next_page', False)
                    after = data.get('page_info', {})\
                                .get('end_cursor', None)
                    if not after is None:
                        self._variables[target]['after'] = after
                    restart = 'message' in response and response['message'] == 'rate limited'
                except json.decoder.JSONDecodeError as e:
                    repeat = False
                    restart = False
                    profiles = []
                yield profiles, restart

    def __len__(self):
        return len(self._items)

    def add(self, items: list):
        """ """
        for item in items:
            username = item.get('node', {}).get('username')
            self._items[username] = item.get('node')

    def save(self):
        with open(self._followers_output, 'w') as f:
            json.dump(self._items, f)
        afters = dict()
        for target, variables in self._variables.items():
            if variables.get('after'):
                afters[target] = variables.get('after')
        with open(self._config_afters_cache, 'w') as f:
            yaml.dump(afters, f, default_flow_style=False)


class Images:
    """
    """

    pass


class Comments:
    """
    """

    pass