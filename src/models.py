import argparse
import yaml
import os.path
import time

import handlers


class Config:
    """
    """

    __PATH = 'config.yml'

    __validation_rules = dict(required=('sessionid', 'followers_afters_cache', 'followers_output', 'host', 'action', 'user_agent'),
                              type=(('sessionid', str),
                                    ('followers_afters_cache', str),
                                    ('followers_output', str),
                                    ('host', str),
                                    ('targets', list),
                                    ('action', str),
                                    ('user_agent', str)),
                              func=(('followers_afters_cache', lambda x: os.path.exists(x)),
                                    ('followers_output', lambda x: os.path.exists(x)),
                                    ('sessionid', lambda x: len(x.split(':')) == 3)),
                              in_=dict(action=('followers', 'images', 'comments')))

    @classmethod
    def __read_config(cls):
        """ """
        for key, value in yaml.load(open(cls.__PATH), Loader=yaml.FullLoader).items():
            setattr(cls, key, value)
        return cls

    @classmethod
    def __read_arguments(cls):
        """ """
        parser = argparse.ArgumentParser()
        parser.add_argument('action')
        cls.action = parser.parse_args().action
        return cls

    @classmethod
    def __validate(cls):
        """ """
        for key in cls.__validation_rules['required']:
            if not hasattr(cls, key):
                raise Exception(f'Config: parameter `{key}` not found')
        for key, value in cls.__validation_rules['type']:
            if not isinstance(getattr(cls, key), value):
                raise Exception(f'Config: Wrong type for {key}, expected `{value}`')
        for key, func in cls.__validation_rules['func']:
            if not func(getattr(cls, key)):
                raise Exception(f'Config: Validation not passed for `{key}`')
        for key, values in cls.__validation_rules['in_'].items():
            if not getattr(cls, key) in values:
                raise Exception(f'Config: Not allowed action for `{key}`')
        return cls

    def __init__(self):
        """ """
        self.__read_config()\
            .__read_arguments()\
            .__validate()

    def __getattr__(self, key: str):
        """ """
        return self.__dict__.get(key)


class Worker:
    """
    """

    _handler = None

    def __init__(self, config: Config):
        """ """
        self._config = config

    @property
    def handler(self):
        """ """
        return self._handler

    @handler.setter 
    def handler(self, value):
        """ """
        if isinstance(value, handlers.Followers):
            self._handler = value

    def run(self):
        """ """
        try:
            for items, restart in self._handler:
                self._handler.add(items)
                print(len(self._handler))
            self._handler.save()
            if restart:
                time.sleep(60 * 5)
                self.run()
        except Exception as e:
            self._handler.save()
