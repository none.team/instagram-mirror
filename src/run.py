import models
import handlers

config = models.Config()
worker = models.Worker(config=config)
if config.action == 'followers':
    worker.handler = handlers.Followers(config=config)
# if config.action == 'images':
#     worker.handler = handlers.Images(config=config)
# if config.action == 'comments':
#     worker.handler = handlers.Comments(config=config)
worker.run()
